#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
import optparse
import re


XGETTEXT = "xgettext"
MSGMERGE = "msgmerge"
DOMAIN = "pythonsudoku"
POTFILES = "POTFILES"
POTFILE = "pythonsudoku.pot"

MSGFMT = "msgfmt"
MOBASEDIR = "../locale/"
MOFILE = "pythonsudoku.mo"


def linguas():
    linguas = []
    listdir = os.listdir(".")
    for file in listdir:
        if re.search("^.*\.po$", file):
            linguas.append(file[:-3])
    return linguas

def update():
    # update template file
    os.system("%s --language=Python --default-domain=%s --directory=.. --sort-by-file --files-from=%s --keyword=_  --output=%s" % (XGETTEXT, DOMAIN, POTFILES, POTFILE))

    # update already created translations
    for lingua in linguas():
        os.system("%s --quiet --update --no-wrap %s %s" % (MSGMERGE, lingua + ".po", POTFILE))

def check():
    for lingua in linguas():
        f = file(lingua + ".po", "r")

        lineno = 0
        fuzzy = False
        msgid = ""
        changes = False
        for line in f:
            line = line.strip()
            lineno += 1

            if re.search("^#, fuzzy", line):
                fuzzy = True
            elif re.search("^msgid \".*\"$", line):
                msgid = line[7:-1]
                if fuzzy and msgid != "":
                    if not changes:
                        print "Changes in %s:" % (lingua + ".po")
                        changes = True
                    print "\tLine %s: \"%s\" fuzzy translation" % (lineno,
                                                                   msgid)
                fuzzy = False
            elif re.search("^msgstr \"\"$", line) and msgid != "":
                if not changes:
                    print "Changes in %s:" % (lingua + ".po")
                    changes = True
                print "\tLine %s: \"%s\" not translated" % (lineno, msgid)

        f.close()

def install():
    for lingua in linguas():
        modir = os.path.join(MOBASEDIR, lingua, "LC_MESSAGES")
        try:
            os.makedirs(modir)
        except OSError:
            pass
        os.system("%s --output=%s %s" % (MSGFMT,
                                         os.path.join(modir, DOMAIN + ".mo"),
                                         lingua + ".po"))

usage = "%prog [options]"
parser = optparse.OptionParser(usage=usage)
parser.add_option("-u", "--update", action="store_true", dest="update",
                  help="update pythonsudoku.pot")
parser.add_option("-c", "--check", action="store_true", dest="check",
                  help="search for untranslated strings in translations")
parser.add_option("-i", "--install", action="store_true", dest="install",
                  help="install the translations")

status = 0

(options, args) = parser.parse_args()
if options.update:
    update()
elif options.check:
    check()
elif options.install:
    install()
else:
    parser.print_help()
    status = 1

sys.exit(status)
